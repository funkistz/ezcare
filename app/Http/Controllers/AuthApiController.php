<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthApiController extends Controller
{
    /**
     * Login customer
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {

        $email = $request->input('email');
        $password = $request->input('password');

        if (empty($email) || empty($password)) {

            return response()->json([
                'status' => '400',
                'message' => 'Please provide username and password',
            ], 400);
        }

        $users = User::where('email', $email)->get();

        if (count($users) <= 0) {

            return response()->json([
                'status' => '400',
                'message' => 'Username does not exist.',
            ], 400);
        }

        $user = $users[0];

        if (!Hash::check($password, $user->password)) {

            return response()->json([
                'status' => '400',
                'message' => 'Password is incorrect.',
            ], 400);
        }

        return response()->json([
            'status' => '200',
            'message' => 'Login success',
            'data' => $user
        ], 200);
    }
}
