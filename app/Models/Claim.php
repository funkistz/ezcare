<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'claim';

    protected $fillable = [
        'claimtype',
        'claimdate',
        'customer',
        'mileage',
        'amount',
        'description1',
        'description2',
        'workshop',
        'cre_dte_tme',
        'cre_by',
        'mod_dte_tme',
        'mod_by'
    ];
}
