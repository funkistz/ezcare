<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Claim;
use App\Http\Controllers\ClaimController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\AuthApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('tokens/create', function (Request $request) {
    $token = $request->user()->createToken($request->token_name);

    return ['token' => $token->plainTextToken];
});


Route::resource('claim', ClaimController::class);
Route::resource('banner', BannerController::class);


//auth api
Route::post('/login', [AuthApiController::class, 'login']);
